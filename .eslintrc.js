module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: 'airbnb-base',
  overrides: [
  ],
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module',
  },
  rules: {
    quotes: ['warn', 'double', { avoidEscape: true }],
    'indent': ['error', 'tab'],
    'padded-blocks': ['error', {blocks: 'always'}],
    'no-console': 'off' ,
    'object-curly-spacing': ["error", "always", { "arraysInObjects": false }],
    'linebreak-style': ['error', 'windows'],
    'prefer-arrow-callback': [ "warn", { "allowNamedFunctions": true } ],
    'no-multiple-empty-lines': ['error', { max: 2, maxBOF: 0, maxEOF: 1 }],
    'lines-between-class-members': ["error", "never"],
    'max-classes-per-file': ['error', 3],
    'no-unused-vars': ['warn']
  },
};
