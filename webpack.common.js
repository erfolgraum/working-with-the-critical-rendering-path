const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CopyPlugin = require("copy-webpack-plugin");




module.exports = {

    entry: path.resolve(__dirname, "src", "main.js"),

    output: {
      path: path.resolve(__dirname, "build"),
      clean: true,
      filename: "bundle.js",
      publicPath: '/'
    },

    plugins: [
      new HtmlWebpackPlugin({
        template: path.resolve(__dirname, "src", "index.html"),
      }),
      new MiniCssExtractPlugin({
        filename: "[name]-[contenthash].css",
      }),

      new CopyPlugin({
        patterns: [
          // {from:'./src/assets/images', to:'images'}
          {
            from: "./src/assets/images/*.png",
            to({ context }) {
              return "./[name][ext]";
            },
          },
        ],
      }),
    ],
    
    module: {
      rules: [
        {
          test: /\.css$/,
          use: [MiniCssExtractPlugin.loader, "css-loader"],
        },

        { test: /\.(png|jpe?g)$/i,
          use: [
            {
              loader: 'image-webpack-loader',
              options: {
                mozjpeg: {
                  progressive: true,
                },
                // optipng.enabled: false will disable optipng
                optipng: {
                  enabled: false,
                },
                pngquant: {
                  quality: [0.65, 0.90],
                  speed: 4
                },
                // the webp option will enable WEBP
                webp: {
                  quality: 75
                }
              }
            },
          ],
        }
      ],
    },
};




